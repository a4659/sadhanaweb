<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menucom extends CI_Controller
{
    public function index()
    {
        $data['title'] = 'Menu Company';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu_company')->result_array();

        //echo 'Selamat Datang' . $data['user']['name'];

        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {
            // $this->load->view('templates/header', $data);
            // $this->load->view('templates/sidebar', $data);
            // $this->load->view('templates/topbar', $data);
            $this->load->view('user/index', $data);
            //$this->load->view('templates/footer');
            //$this->load->view('user/index', $data);
        } else {
            $this->db->insert('user_menu_company', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New menu added!</div>');
            redirect('menucom');
        }
    }

    public function submenu()
    {
        $data['title'] = 'Menu Sadhana Company';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu_company')->result_array();

        //echo 'Selamat Datang' . $data['user']['name'];

        $this->form_validation->set_rules('menu', 'Menu', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('url', 'Url', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menucom/index', $data);
            $this->load->view('templates/footer');
            //$this->load->view('user/index', $data);
        } else {
            $data = [
                'menu' => $this->input->post('menu'),
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active'),
            ];
            $this->db->insert('user_menu_company', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Menu added!</div>');
            redirect('menucom/submenu');
        }
    }
}




//public function deletesubmenu($id)
//{
  //  $this->db->where('id', $id);
    //$this->db->delete('user_menu_company');
    //$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Delete sub menu success!</div>');
    //redirect('menucom/submenu');
//}
//}
public function deletesubmenu($id)
{
    $model = new Menu_model();
    $id = $this->request->getPost('id');
    $model->deleteMenu($id);
    return redirect()->to('/user_menu_company');