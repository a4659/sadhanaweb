<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'My Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['userwe'] = $this->db->get('user')->result_array();

        //echo 'Selamat Datang' . $data['user']['name'];

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/index', $data);
            $this->load->view('templates/footer');
            //$this->load->view('user/index', $data);
        } else {
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => $this->input->post('role_id', true),
                'is_active' => $this->input->post('is_active', true),
                'date_created' => time()
            ];

            $this->db->insert('user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat Akun Anda Sudah Terdaftar. Silakan Login Kembali!</div>');
            redirect('user');
        }
    }

    public function edit()
    {
        $data['title'] = 'Edit Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['userwe'] = $this->db->get('user')->result_array();

        // $this->form_validation->run() == false) {
        $this->form_validation->set_rules('name', 'Full Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            //cek gambar yang di pilih
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/profile/';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['image'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    echo $this->upload->dispay_errors();
                }
            }
            // var_dump($upload_image);
            // die;

            $this->db->set('name', $name);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun Anda Sukses Dirubah!</div>');
            redirect('user');
        }

        //$this->load->view('user/index', $data);

    }

    // public function datauser()
    // {
    //     $data['title'] = 'Data User';
    //     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    //     //echo 'Selamat Datang' . $data['user']['name'];
    //     $data['userwe'] = $this->db->get('user')->result_array();

    //     //echo 'Selamat Datang' . $data['user']['name'];

    //     $this->form_validation->set_rules('name', 'Name', 'required');
    //     $this->form_validation->set_rules('email', 'Email', 'required');
    //     $this->form_validation->set_rules('image', 'Image', 'required');
    //     $this->form_validation->set_rules('password', 'Password', 'required');
    //     $this->form_validation->set_rules('role_id', 'Role_id', 'required');
    //     $this->form_validation->set_rules('is_active', 'Is_active', 'required');

    //     if ($this->form_validation->run() == false) {
    //         $this->load->view('templates/header', $data);
    //         $this->load->view('templates/sidebar', $data);
    //         $this->load->view('templates/topbaruser', $data);
    //         $this->load->view('user/datauser', $data);
    //         $this->load->view('templates/footer');
    //         //$this->load->view('user/index', $data);
    //     } else {
    //         $this->db->insert('user', [
    //             'name' => $this->input->post('name'),
    //             'email' => $this->input->post('email'),
    //             'image' => 'default.jpg',
    //             'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
    //             'role_id' => $this->input->post('role_id'),
    //             'is_active' => $this->input->post('is_active'),
    //         ]);
    //         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New User added!</div>');
    //         redirect('user');
    //     }
    // }
}
